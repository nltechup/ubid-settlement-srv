﻿namespace ubid_settlement_common.Constants
{
    public class Classes
    {
        public const string BindQueue = "QueueSettings";
        public const string BindDbSettings = "SettlementDatabaseSettings";
        public const string BindSettlement = "SettlementSettings";

    }
}
