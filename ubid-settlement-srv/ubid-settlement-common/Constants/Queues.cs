﻿namespace ubid_settlement_common.Constants
{
    public static class Queues
    {
        public static readonly string Settlement_Offer_Create = "SettlementOfferCreate";
        public static readonly string Settlement_Offer_Update = "SettlementOfferUpdate";
        public static readonly string Settlement_Offer_Delete = "SettlementOfferDelete";

        public static readonly string Settlement_Bid_Create = "SettlementBidCreate";
        public static readonly string Settlement_Bid_Update = "SettlementBidUpdate";
        public static readonly string Settlement_Bid_Delete = "SettlementBidDelete";

        public static readonly string Settlement_Status = "SettlementStatusQueue";
        public static readonly string Settlement_Result = "SettlementResultQueue";
    }
}
