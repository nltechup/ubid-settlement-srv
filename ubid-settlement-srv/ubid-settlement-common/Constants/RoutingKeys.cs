﻿namespace ubid_settlement_common.Constants
{
    public static class RoutingKeys
    {
        public static readonly string Bid_Create = "bid.create";
        public static readonly string Bid_Update = "bid.update";
        public static readonly string Bid_Delete = "bid.delete";
        public static readonly string Offer_Create = "offer.create";
        public static readonly string Offer_Update = "offer.update";
        public static readonly string Offer_Delete = "offer.delete";
        public static readonly string Settlement_Status = "settlement.status";
        public static readonly string Settlement_Result = "settlement.result";
        public static readonly string Email_Send = "email.send";
    }
}
