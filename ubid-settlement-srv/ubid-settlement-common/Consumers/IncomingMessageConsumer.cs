﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ubid_settlement_common.Consumers
{
    public abstract class IncomingMessageConsumer : EventingBasicConsumer
    {
        public IncomingMessageConsumer(IModel channel)
            : base(channel)
        {
        }
        public abstract override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered,
            string exchange, string routingKey, IBasicProperties properties, byte[] body);
    }
}
