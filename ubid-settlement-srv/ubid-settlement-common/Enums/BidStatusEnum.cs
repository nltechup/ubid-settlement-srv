﻿namespace ubid_settlement_common.Enums
{
    public enum BidStatus
    {
        Created,
        Processing,
        Settled
    }
}
