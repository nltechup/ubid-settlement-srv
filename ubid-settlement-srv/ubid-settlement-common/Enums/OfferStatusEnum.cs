﻿namespace ubid_settlement_common.Enums
{
    public enum OfferStatus
    {
        Created = 1,
        Processing = 2,
        Settled = 3,
        Expired = 4
    }
}
