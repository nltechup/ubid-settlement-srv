﻿namespace ubid_settlement_common.Enums
{
    public enum StrategyType
    {
        MaxQuantity,
        MaxValue,
        MaxProfit

    }
}
