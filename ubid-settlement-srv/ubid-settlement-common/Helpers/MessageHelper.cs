﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ubid_settlement_common.Helpers
{
    public class MessageHelper
    {
        /// <summary>
        /// Serialize an object, so that it can be sent as a message in RabbitMQ.
        /// </summary>
        /// <typeparam name="T"> Type of object to be serialized. </typeparam>
        /// <param name="obj"> The object that will be serialized. </param>
        /// <returns> A byte array that can be sent as a message </returns>
        public static byte[] SerializeForSending<T>(T obj) where T : new()
        {
            var jsonString = JsonConvert.SerializeObject(obj);
            return Encoding.UTF8.GetBytes(jsonString);
        }

        /// <summary>
        /// Deserialize a byte array into recieved from RabbitMQ Queue to a .Net Object
        /// </summary>
        /// <typeparam name="T"> Type of object to desrialize to </typeparam>
        /// <param name="body"> RabbitMQ message body </param>
        /// <returns></returns>
        public static T DeserializeMessage<T>(byte[] body) where T : new()
        {
            var jsonString = Encoding.UTF8.GetString(body);
            return JsonConvert.DeserializeObject<T>(jsonString);
        }
    }
}
