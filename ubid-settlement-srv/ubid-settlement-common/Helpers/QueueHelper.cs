﻿using RabbitMQ.Client;

namespace ubid_settlement_common.Helpers
{
    public class QueueHelper
    {
        public void InitializeQueue(IModel channel, string queueName)
        {
            channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false);
        }

        public void InitializeExchange(IModel channel, string exchangeName)
        {
            channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, durable: true, autoDelete: false);
        }

        public void BindQueueToExchange(IModel channel, string exchange, string queue, string routingKey)
        {
            channel.QueueBind(queue, exchange, routingKey);
        }
    }
}
