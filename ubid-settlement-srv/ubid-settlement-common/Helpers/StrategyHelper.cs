﻿using System;
using ubid_settlement_common.Enums;

namespace ubid_settlement_common.Helpers
{
    public class StrategyHelper
    {
        public static StrategyType GetStrategy(string sType)
        {
            StrategyType result = (StrategyType)Enum.Parse(typeof(StrategyType), sType);
            return result;
        }

    }
}