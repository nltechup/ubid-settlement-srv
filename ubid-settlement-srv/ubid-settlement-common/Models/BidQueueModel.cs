﻿using System;

namespace ubid_settlement_common.Models
{
    //public class BidQueueModel
    //{
    //    public string cat_uuid { get; set; }

    //    public int quantity { get; set; }

    //    public DateTime end { get; set; }
    //    public decimal price { get; set; }

    //    public DateTime createdAt { get; set; }
    //    public DateTime updatedAt { get; set; }
    //    public string _id { get; set; }

    //}


    public class BidQueueModel
    {
        public string id { get; set; }
        public BidOffer offer { get; set; }
        public float price { get; set; }
        public int qty { get; set; }
        public Createdat createdAt { get; set; }
        public string bidderId { get; set; }
    }

    public class BidOffer
    {
        public string id { get; set; }
        public string product { get; set; }
        public string description { get; set; }
        public string photoUrl { get; set; }
    }

    public class Createdat
    {
        public string month { get; set; }
        public int dayOfYear { get; set; }
        public string dayOfWeek { get; set; }
        public int nano { get; set; }
        public int year { get; set; }
        public int monthValue { get; set; }
        public int dayOfMonth { get; set; }
        public int hour { get; set; }
        public int minute { get; set; }
        public int second { get; set; }
        public Chronology chronology { get; set; }
    }

    public class Chronology
    {
        public string calendarType { get; set; }
        public string id { get; set; }
    }

}
