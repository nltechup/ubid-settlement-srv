﻿using System;

namespace ubid_settlement_common.Models
{
    public class OfferQueueModel
    {
        public string cat_uuid { get; set; }
        public string product { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
        public string photo_url { get; set; }
        public DateTime end { get; set; }
        public decimal price { get; set; }
        public string user_uuid { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string _id { get; set; }
        public string user_email { get; set; }
    }


}
