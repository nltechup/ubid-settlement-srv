﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using ubid_settlement_common.Constants;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Models;

namespace ubid_settlement_common.RabbitMQ
{
    public class RabbitMQValidations
    {
        private ILogger<RabbitMQValidations> _logger;
        private QueueSettings _rabbitConfig = new QueueSettings();

        private IConnection _connection;
        private IModel _channel;

        public RabbitMQValidations(QueueSettings queueSettings, ILogger<RabbitMQValidations> logger)
        {
            _logger = logger;
            _rabbitConfig = queueSettings;

            var factory = new ConnectionFactory()
            {
                HostName = _rabbitConfig.Rabbit_HostName,
                Port = _rabbitConfig.Rabbit_Port,
                UserName = _rabbitConfig.Rabbit_Username,
                Password = _rabbitConfig.Rabbit_Password
            };
            RabbitMQHelper.WaitForRabbitMq(factory);

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        public void ValidateQueues()
        {
            _logger.LogInformation($"Checking rabbtiMQ infrastructure");
            // exchanges
            _channel.ExchangeDeclare(Exchanges.UBID_Exchange, ExchangeType.Topic, durable: true, autoDelete: false);
            // queues
            _channel.QueueDeclare(queue: Queues.Settlement_Offer_Create, durable: true, exclusive: false, autoDelete: false);
            _channel.QueueDeclare(queue: Queues.Settlement_Offer_Update, durable: true, exclusive: false, autoDelete: false);
            _channel.QueueDeclare(queue: Queues.Settlement_Offer_Delete, durable: true, exclusive: false, autoDelete: false);

            _channel.QueueDeclare(queue: Queues.Settlement_Bid_Create, durable: true, exclusive: false, autoDelete: false);
            _channel.QueueDeclare(queue: Queues.Settlement_Bid_Update, durable: true, exclusive: false, autoDelete: false);
            _channel.QueueDeclare(queue: Queues.Settlement_Bid_Delete, durable: true, exclusive: false, autoDelete: false);

            _channel.QueueDeclare(queue: Queues.Settlement_Status, durable: true, exclusive: false, autoDelete: false);
            _channel.QueueDeclare(queue: Queues.Settlement_Result, durable: true, exclusive: false, autoDelete: false);

            //bindings
            _channel.QueueBind(Queues.Settlement_Offer_Create, Exchanges.UBID_Exchange, RoutingKeys.Offer_Create);
            _channel.QueueBind(Queues.Settlement_Offer_Update, Exchanges.UBID_Exchange, RoutingKeys.Offer_Update);
            _channel.QueueBind(Queues.Settlement_Offer_Delete, Exchanges.UBID_Exchange, RoutingKeys.Offer_Delete);

            _channel.QueueBind(Queues.Settlement_Bid_Create, Exchanges.UBID_Exchange, RoutingKeys.Bid_Create);
            _channel.QueueBind(Queues.Settlement_Bid_Update, Exchanges.UBID_Exchange, RoutingKeys.Bid_Update);
            _channel.QueueBind(Queues.Settlement_Bid_Delete, Exchanges.UBID_Exchange, RoutingKeys.Bid_Delete);

            _channel.QueueBind(Queues.Settlement_Status, Exchanges.UBID_Exchange, RoutingKeys.Settlement_Status);
            _channel.QueueBind(Queues.Settlement_Result, Exchanges.UBID_Exchange, RoutingKeys.Settlement_Result);

            _logger.LogInformation($"RabbitMQ infrastructure ready");
        }

    }
}
