﻿namespace ubid_settlement_common.Models
{
    public class SettlementDatabaseSettings
    {
        public string Mongo_Host { get; set; }
        public int Mongo_Port { get; set; }
        public string Mongo_Username { get; set; }
        public string Mongo_Password { get; set; }
        public string Mongo_Database { get; set; }
        public string Mongo_Auth_Db { get; set; }
        public string Mongo_Auth_Mechanism { get; set; }

        public string LocalConnection
        {
            get
            {
                return "mongodb://localhost:27017";
            }
        }
        public string GetConnectionString
        {
            get
            {
                return $@"mongodb://{Mongo_Username}:{Mongo_Password}@{Mongo_Host}:{Mongo_Port}/{Mongo_Database}";
            }
        }
    }
}
