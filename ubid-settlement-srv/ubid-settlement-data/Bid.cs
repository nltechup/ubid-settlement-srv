﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using ubid_settlement_common.Enums;

namespace ubid_settlement_data
{
    public class Bid
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public string BidId { get; set; }
        public string OfferId { get; set; }
        public int Quantity { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime EndAt { get; set; }
        public decimal Price { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime UpdatedAt { get; set; }
        public string BidUserEmail { get; set; }
        public BidStatus BidStatus { get; set; }

        public decimal BidValue
        {
            get
            {
                return Quantity * Price;
            }
        }
    }

}
