﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using ubid_settlement_common.Enums;

namespace ubid_settlement_data
{
    public class Offer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public string OfferId { get; set; }
        public int Quantity { get; set; }
        public int Remaining { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime EndAt { get; set; }
        public decimal Price { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime UpdatedAt { get; set; }
        public string OfferUserEmail { get; set; }
        public OfferStatus OfferStatus { get; set; }

    }
}
