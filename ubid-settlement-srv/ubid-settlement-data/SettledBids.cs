﻿namespace ubid_settlement_data
{
    public class SettledBids
    {
        public string BidId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
