﻿using System.Collections.Generic;

namespace ubid_settlement_data
{
    public class Settlement
    {
        public Settlement()
        {
            Bids = new List<SettledBids>();
        }
        public string OfferId { get; set; }
        public int OfferQuantity { get; set; }
        public List<SettledBids> Bids { get; set; }
    }
}
