﻿using System;
using System.Collections.Generic;
using ubid_settlement_common.Enums;
using ubid_settlement_data;

namespace ubid_settlement_repository
{
    public interface ISettlementRepository
    {
        void AddOffer(Offer newOffer);
        bool UpdateOffer(Offer updateOffer);
        bool DeleteOffer(Offer updateOffer);
        IList<Offer> GetOffersByDateTime(DateTime pTime);
        IList<Bid> GetBidsByOfferId(List<string> offerIds);
        void SetOffersStatus(List<string> offerIds, OfferStatus status);
        bool UpdateBid(Bid bid);
        void AddBid(Bid newBid);
    }
}
