﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using ubid_settlement_common.Enums;
using ubid_settlement_common.Models;
using ubid_settlement_data;

namespace ubid_settlement_repository
{
    public class SettlementRepository : ISettlementRepository
    {
        private ILogger<SettlementRepository> _logger;
        private readonly IMongoDatabase _database = null;
        private SettlementDatabaseSettings _dbSettings = new SettlementDatabaseSettings();
        public SettlementRepository(ILogger<SettlementRepository> logger, SettlementDatabaseSettings dbSettings)
        {
            _logger = logger;
            _dbSettings = dbSettings;

            var settings = new MongoClientSettings();
            if (!string.IsNullOrEmpty(_dbSettings.Mongo_Username))
            {
                MongoIdentity identity = new MongoInternalIdentity(_dbSettings.Mongo_Auth_Db, _dbSettings.Mongo_Username);
                MongoIdentityEvidence evidence = new PasswordEvidence(_dbSettings.Mongo_Password);

                var credential = new MongoCredential(_dbSettings.Mongo_Auth_Mechanism, identity, evidence);

                //var credential = MongoCredential.CreateCredential( _dbSettings.Mongo_Database, _dbSettings.Mongo_Username, _dbSettings.Mongo_Password);

                settings.Credential = credential;
            }

            MongoServerAddress srvAddres = new MongoServerAddress(_dbSettings.Mongo_Host, _dbSettings.Mongo_Port);
            settings.Server = srvAddres;
            var dbClient = new MongoClient(settings);

            _database = dbClient.GetDatabase(_dbSettings.Mongo_Database);

        }

        public void AddOffer(Offer newOffer)
        {
            _logger.LogInformation($"Add new offer {newOffer.OfferId}-qty:{newOffer.Quantity}-price{newOffer.Price}");

            IMongoCollection<Offer> offers = _database.GetCollection<Offer>("Offers");
            offers.InsertOne(newOffer);
        }

        public bool UpdateOffer(Offer updateOffer)
        {
            _logger.LogInformation($"Updating the offer {updateOffer.OfferId}-qty:{updateOffer.Quantity}-price{updateOffer.Price}");

            var filterId = Builders<Offer>.Filter.Eq("OfferId", updateOffer.OfferId);
            IMongoCollection<Offer> offers = _database.GetCollection<Offer>("Offers");
            var updated = offers.FindOneAndReplace(filterId, updateOffer);
            return updated != null;
        }

        public bool DeleteOffer(Offer deleteOffer)
        {
            _logger.LogInformation($"Deleting the offer {deleteOffer.OfferId}-qty:{deleteOffer.Quantity}-price{deleteOffer.Price}");

            var filterId = Builders<Offer>.Filter.Eq("OfferId", deleteOffer.OfferId);
            IMongoCollection<Offer> offers = _database.GetCollection<Offer>("Offers");
            var updated = offers.FindOneAndDelete(filterId);
            return updated != null;
        }

        public IList<Offer> GetOffersByDateTime(DateTime pTime)
        {
            IMongoCollection<Offer> offers = _database.GetCollection<Offer>("Offers");
            //var filterDate = Builders<Offer>.Filter.Lte(x => x.EndAt, pTime);

            var result = offers.Find(x => x.EndAt <= pTime && x.OfferStatus == OfferStatus.Created).ToList();
            return result;
        }

        public void SetOffersStatus(List<string> offerIds, OfferStatus status)
        {
            IMongoCollection<Offer> offers = _database.GetCollection<Offer>("Offers");
            var filter = Builders<Offer>.Filter.In(x => x.OfferId, offerIds);
            UpdateDefinition<Offer> update = Builders<Offer>.Update.Set(s => s.OfferStatus, OfferStatus.Processing);

            //IList<Offer> result = offers.Find(f => offerIds.Contains(f.OfferId)).ToList();

            offers.UpdateMany(filter, update);

        }

        public IList<Bid> GetBidsByOfferId(List<string> offerIds)
        {
            IMongoCollection<Bid> bids = _database.GetCollection<Bid>("Bids");

            var result = bids.Find(f => offerIds.Contains(f.OfferId)).ToList();

            return result;
        }

        public bool UpdateBid(Bid bid)
        {
            _logger.LogInformation($"Updating the bid {bid.BidId}-qty:{bid.Quantity}-price{bid.Price}");

            var filterId = Builders<Bid>.Filter.Eq("BidId", bid.BidId);
            IMongoCollection<Bid> bids = _database.GetCollection<Bid>("Bids");
            var updated = bids.FindOneAndReplace(filterId, bid);
            return updated != null;
        }

        public void AddBid(Bid newBid)
        {
            _logger.LogInformation($"Add new bid {newBid.BidId}-qty:{newBid.Quantity}-price: {newBid.Price}");

            IMongoCollection<Bid> bids = _database.GetCollection<Bid>("Bids");
            bids.InsertOne(newBid);
        }
    }
}
