﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using ubid_settlement_common.Constants;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Models;
using ubid_settlement_data;
using ubid_settlement_repository;

namespace ubid_settlement_srv.Consumers
{
    public class BidCreateConsumer : GenericConsumer
    {
        private IModel _channel;
        private ISettlementRepository _repository;
        private IMapper _mapper;
        private ILogger<BidCreateConsumer> _logger;
        public static string QueueName
        {
            get
            {
                return Queues.Settlement_Bid_Create;
            }
        }
        public static string RoutingKey
        {
            get
            {
                return RoutingKeys.Bid_Create;
            }
        }

        /*, ISettlementRepository repository, IMapper mapper, ILogger<BidCreateConsumer> logger*/
        public BidCreateConsumer(IModel channel, ISettlementRepository repository, IMapper mapper, ILogger<BidCreateConsumer> logger) : base(channel)
        {
            _channel = channel;
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            try
            {
                BidQueueModel offerMessage = MessageHelper.DeserializeMessage<BidQueueModel>(body);
                Bid newBid = _mapper.Map<Bid>(offerMessage);
                newBid.BidStatus = ubid_settlement_common.Enums.BidStatus.Created;
                _repository.AddBid(newBid);
                _channel.BasicAck(deliveryTag, false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error consuming bid create queue {ex.Message}");
            }

        }
    }
}
