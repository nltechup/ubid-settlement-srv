﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using ubid_settlement_common.Constants;
using ubid_settlement_repository;

namespace ubid_settlement_srv.Consumers
{
    public class BidDeleteConsumer : GenericConsumer
    {
        private IModel _channel;
        private ISettlementRepository _repository;
        private IMapper _mapper;
        private ILogger<BidDeleteConsumer> _logger;
        public static string QueueName
        {
            get
            {
                return Queues.Settlement_Bid_Delete;
            }
        }
        public static string RoutingKey
        {
            get
            {
                return RoutingKeys.Bid_Delete;
            }
        }
        public BidDeleteConsumer(IModel channel, ISettlementRepository repository, IMapper mapper, ILogger<BidDeleteConsumer> logger) : base(channel)
        {
            _channel = channel;
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
