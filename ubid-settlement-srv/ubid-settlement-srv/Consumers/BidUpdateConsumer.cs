﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using ubid_settlement_common.Constants;
using ubid_settlement_repository;

namespace ubid_settlement_srv.Consumers
{
    public class BidUpdateConsumer : GenericConsumer
    {
        private IModel _channel;
        private ISettlementRepository _repository;
        private IMapper _mapper;
        private ILogger<BidUpdateConsumer> _logger;
        public static string QueueName
        {
            get
            {
                return Queues.Settlement_Bid_Update;
            }
        }
        public static string RoutingKey
        {
            get
            {
                return RoutingKeys.Bid_Update;
            }
        }

        /*, ISettlementRepository repository, IMapper mapper, ILogger<BidCreateConsumer> logger*/
        public BidUpdateConsumer(IModel channel, ISettlementRepository repository, IMapper mapper, ILogger<BidUpdateConsumer> logger) : base(channel)
        {
            _channel = channel;
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
