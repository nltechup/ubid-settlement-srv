﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ubid_settlement_srv.Consumers
{
    public class GenericConsumer : EventingBasicConsumer
    {
        public GenericConsumer(IModel channel) : base(channel)
        {

        }
    }
}
