﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Models;
using ubid_settlement_data;
using ubid_settlement_repository;
using ubid_settlement_srv.Listeners;

namespace ubid_settlement_srv.Consumers
{
    public class OfferDeleteConsumer : IncomingMessageConsumer
    {
        private readonly IModel _channel;
        private readonly ISettlementRepository _repository;
        private readonly IMapper _mapper;
        private readonly ILogger<OfferDeleteListener> _logger;

        public OfferDeleteConsumer(IModel channel, ISettlementRepository repository, IMapper mapper, ILogger<OfferDeleteListener> logger) : base(channel)
        {
            _channel = channel;
            _repository = repository;
            _mapper = mapper;
            _logger = logger;
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            try
            {
                OfferQueueModel offerMessage = MessageHelper.DeserializeMessage<OfferQueueModel>(body);
                Offer newOffer = _mapper.Map<Offer>(offerMessage);
                newOffer.OfferStatus = ubid_settlement_common.Enums.OfferStatus.Created;
                bool deleted = _repository.DeleteOffer(newOffer);
                _channel.BasicAck(deliveryTag, false);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error consuming offer delete queue {ex.Message}");
            }
        }
    }
}
