﻿using System.Collections.Generic;
using ubid_settlement_data;
using ubid_settlement_repository;

namespace ubid_settlement_srv.Interfaces
{
    public interface ISettlementStrategy
    {
        List<Settlement> ProcessSettlement(IList<Offer> offers, IList<Bid> bids, ISettlementRepository _repository);
    }
}
