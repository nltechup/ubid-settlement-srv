﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using ubid_settlement_common.Constants;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Models;
using ubid_settlement_repository;
using ubid_settlement_srv.Consumers;

namespace ubid_settlement_srv.Listeners
{
    public class GenericListener<C> where C : GenericConsumer
    {
        private ILogger<C> _logger;
        private IModel _channel;
        private ISettlementRepository _repository;
        private IMapper _mapper;
        QueueSettings _queueSettings = new QueueSettings();

        public GenericListener(ILogger<C> logger, QueueSettings queueSettings, ISettlementRepository repository, IMapper mapper)
        {
            _logger = logger;

            _queueSettings = queueSettings;            

            var factory = new ConnectionFactory()
            {
                HostName = _queueSettings.Rabbit_HostName,
                Port = _queueSettings.Rabbit_Port,
                UserName = _queueSettings.Rabbit_Username,
                Password = _queueSettings.Rabbit_Password
            };
            RabbitMQHelper.WaitForRabbitMq(factory);

            IConnection _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _repository = repository;
            _mapper = mapper;
        }

        public void Listen()
        {

            var consumer = (C)Activator.CreateInstance(typeof(C), _channel, _repository, _mapper, _logger);
            QueueHelper qHelper = new QueueHelper();

            string routingKey = typeof(C).GetMethod("get_RoutingKey").Invoke(null, null).ToString();
            string queueName = typeof(C).GetMethod("get_QueueName").Invoke(null, null).ToString();
            _logger.LogInformation($"Start listen on {queueName} queue.");


            qHelper.InitializeQueue(_channel, queueName);
            qHelper.InitializeExchange(_channel, Exchanges.UBID_Exchange);
            qHelper.BindQueueToExchange(_channel, Exchanges.UBID_Exchange, queueName, routingKey);

            _channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);
        }
    }
}
