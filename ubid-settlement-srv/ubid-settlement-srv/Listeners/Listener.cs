﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ubid_settlement_srv.Listeners
{
    public class Listener : EventingBasicConsumer
    {
        public Listener(IModel channel) : base(channel)
        {

        }
    }
}
