﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using ubid_settlement_common.Constants;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Models;
using ubid_settlement_repository;
using ubid_settlement_srv.Consumers;

namespace ubid_settlement_srv.Listeners
{
    public class OfferCreateListener
    {
        private readonly ILogger<OfferCreateListener> _logger;
        private readonly ISettlementRepository _repository;
        private readonly IMapper _mapper;
        private readonly IModel channel;


        public OfferCreateListener(ILogger<OfferCreateListener> logger, QueueSettings queueSettings, ISettlementRepository repository, IMapper mapper)
        {
            _logger = logger;
            QueueSettings _queueSettings = new QueueSettings();
            _queueSettings = queueSettings;


            var factory = new ConnectionFactory()
            {
                HostName = _queueSettings.Rabbit_HostName,
                Port = _queueSettings.Rabbit_Port,
                UserName = _queueSettings.Rabbit_Username,
                Password = _queueSettings.Rabbit_Password
            };

            IConnection _connection = factory.CreateConnection();
            channel = _connection.CreateModel();
            _repository = repository;
            _mapper = mapper;
        }


        public void Listen()
        {
            _logger.LogInformation($"Start listen on { Queues.Settlement_Offer_Create} queue.");
            var consumer = new OfferCreateConsumer(channel, _repository, _mapper, _logger);
            QueueHelper qHelper = new QueueHelper();

            string queueName = Queues.Settlement_Offer_Create;

            qHelper.InitializeQueue(channel, queueName);
            qHelper.InitializeExchange(channel, Exchanges.UBID_Exchange);
            qHelper.BindQueueToExchange(channel, Exchanges.UBID_Exchange, queueName, RoutingKeys.Offer_Create);

            channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);
        }
    }
}
