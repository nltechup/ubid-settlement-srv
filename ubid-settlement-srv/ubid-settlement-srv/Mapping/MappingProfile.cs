﻿using AutoMapper;
using System;
using ubid_settlement_common.Models;
using ubid_settlement_data;

namespace ubid_settlement_srv.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<OfferQueueModel, Offer>()
                .ForMember(dest => dest.OfferId, opt => opt.MapFrom(src => src._id))
                .ForMember(dest => dest.OfferUserEmail, opt => opt.MapFrom(src => src.user_email))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.quantity))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.price))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => src.createdAt))
                .ForMember(dest => dest.EndAt, opt => opt.MapFrom(src => src.end))
                .ForMember(dest => dest.UpdatedAt, opt => opt.MapFrom(src => src.updatedAt))
                .ReverseMap();
            CreateMap<BidQueueModel, Bid>()
                .ForMember(dest => dest.BidId, opt => opt.MapFrom(src => src.bidderId))
                .ForMember(dest => dest.OfferId, opt => opt.MapFrom(src => src.offer.id))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.qty))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.price))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => new DateTime(src.createdAt.year, src.createdAt.monthValue, src.createdAt.dayOfMonth)))
                .ReverseMap();
        }
    }
}
