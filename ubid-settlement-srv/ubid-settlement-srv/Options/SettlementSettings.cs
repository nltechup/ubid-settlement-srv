﻿namespace ubid_settlement_srv.Options
{
    public class SettlementSettings
    {
        public int MinutesBeforeCheck { get; set; }
        public string StrategyType { get; set; }
    }
}
