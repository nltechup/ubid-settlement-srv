using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using ubid_settlement_common.Models;
using ubid_settlement_repository;
using ubid_settlement_srv.Options;
using ubid_settlement_srv.Services;



namespace ubid_settlement_srv
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(config =>
                {
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    config.AddJsonFile(@"./config/settlement.json", optional: false, reloadOnChange: true);
                    config.AddCommandLine(args);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;
                    SettlementDatabaseSettings dbSettings = new SettlementDatabaseSettings();
                    QueueSettings qSettings = new QueueSettings();
                    SettlementSettings settings = new SettlementSettings();

                    services.AddOptions();

                    configuration.Bind("SettlementSettings", settings);
                    services.AddSingleton(settings);

                    services.Configure<QueueSettings>(q => configuration.GetSection("QueueSettings").Bind(q));

                    GetDbSettings(configuration, dbSettings);
                    services.AddSingleton(dbSettings);

                    GetQueueSettings(configuration, qSettings);
                    services.AddSingleton(qSettings);

                    // services.AddSingleton<IRabbitMQService, RabbitMQService>();

                    services.AddValidation();
                    services.AddMapping();


                    services.AddSingleton<ISettlementRepository, SettlementRepository>();

                    services.AddListeners();

                    services.AddHostedService<SettlementWorker>();
                });

        private static void GetDbSettings(IConfiguration configuration, SettlementDatabaseSettings dbSettings)
        {
            SettlementDatabaseSettings s = new SettlementDatabaseSettings();
            configuration.Bind("SettlementDatabaseSettings", s);

            dbSettings.Mongo_Host = Environment.GetEnvironmentVariable("MONGO_HOST") ?? s.Mongo_Host;
            dbSettings.Mongo_Port = Environment.GetEnvironmentVariable("MONGO_PORT") == null ? s.Mongo_Port : Convert.ToInt32(Environment.GetEnvironmentVariable("MONGO_PORT"));
            dbSettings.Mongo_Username = Environment.GetEnvironmentVariable("MONGO_USERNAME") ?? s.Mongo_Username;
            dbSettings.Mongo_Database = Environment.GetEnvironmentVariable("MONGO_DATABASE") ?? s.Mongo_Database;
            dbSettings.Mongo_Password = Environment.GetEnvironmentVariable("MONGO_PASSWORD") ?? s.Mongo_Password;
            dbSettings.Mongo_Auth_Db = Environment.GetEnvironmentVariable("MONGO_AUTH_DB") ?? s.Mongo_Auth_Db;
            dbSettings.Mongo_Auth_Mechanism = Environment.GetEnvironmentVariable("MONGO_AUTH_MECHANISM") ?? s.Mongo_Auth_Mechanism;
        }

        private static void GetQueueSettings(IConfiguration configuration, QueueSettings qSettings)
        {
            QueueSettings q = new QueueSettings();
            configuration.Bind("QueueSettings", q);

            qSettings.Rabbit_HostName = Environment.GetEnvironmentVariable("RABBIT_HOSTNAME") ?? q.Rabbit_HostName;
            qSettings.Rabbit_Port = Environment.GetEnvironmentVariable("RABBIT_PORT") == null ? q.Rabbit_Port : Convert.ToInt32(Environment.GetEnvironmentVariable("RABBIT_PORT"));
            qSettings.Rabbit_Username = Environment.GetEnvironmentVariable("RABBIT_USERNAME") ?? q.Rabbit_Username;
            qSettings.Rabbit_Password = Environment.GetEnvironmentVariable("RABBIT_PASSWORD") ?? q.Rabbit_Password;
        }
    }
}

