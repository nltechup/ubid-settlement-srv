﻿using Microsoft.Extensions.DependencyInjection;
using ubid_settlement_srv.Consumers;
using ubid_settlement_srv.Listeners;

namespace ubid_settlement_srv.Services
{
    public static class ListenerService
    {

        public static void AddListeners(this IServiceCollection services)
        {
            services.AddSingleton<OfferCreateListener>();
            services.AddSingleton<OfferUpdateListener>();
            services.AddSingleton<OfferDeleteListener>();

            services.AddSingleton<GenericListener<BidCreateConsumer>>();
            services.AddSingleton<GenericListener<BidUpdateConsumer>>();
            services.AddSingleton<GenericListener<BidDeleteConsumer>>();
        }

    }
}
