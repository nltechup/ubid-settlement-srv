﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ubid_settlement_srv.Mapping;

namespace ubid_settlement_srv.Services
{
    public static class MappingService
    {
        public static void AddMapping(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
