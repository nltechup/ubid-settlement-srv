﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ubid_settlement_common.Models;

namespace ubid_settlement_srv.Services
{
    public static class MongoDbService
    {
        public static void AddMongoServices(this IServiceCollection services, IConfiguration configuration)
        {
            SettlementDatabaseSettings dbsettings = new SettlementDatabaseSettings();
            configuration.GetSection("SettlementDatabaseSettings").Bind(dbsettings);
            services.Configure<SettlementDatabaseSettings>(configuration);
        }
    }
}
