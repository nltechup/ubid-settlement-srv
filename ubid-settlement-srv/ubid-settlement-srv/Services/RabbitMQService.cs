﻿using Microsoft.Extensions.Configuration;
using ubid_settlement_common.RabbitMQ;

namespace ubid_settlement_srv.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        private IConfiguration _configuration;

        public RabbitMQService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
    }
}
