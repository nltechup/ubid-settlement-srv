﻿using Microsoft.Extensions.DependencyInjection;
using ubid_settlement_common.RabbitMQ;

namespace ubid_settlement_srv.Services
{
    public static class ValidationService
    {

        public static void AddValidation(this IServiceCollection services)
        {
            services.AddSingleton<RabbitMQValidations>();
        }
    }
}
