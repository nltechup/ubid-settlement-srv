﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ubid_settlement_common.Constants;
using ubid_settlement_common.Enums;
using ubid_settlement_common.Interfaces;
using ubid_settlement_data;
using ubid_settlement_repository;
using ubid_settlement_srv.Interfaces;
using ubid_settlement_srv.Strategies;

namespace ubid_settlement_srv
{
    public class SettlementJob : ISettlementJob
    {
        private ILogger<ISettlementJob> _logger;
        private ISettlementRepository _repository;
        private IMapper _mapper;
        ISettlementStrategy _settlementStrategy;

        public SettlementJob(ILogger<ISettlementJob> logger, ISettlementRepository repository, IMapper mapper, StrategyType _strategy)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _settlementStrategy = GetSettlementStrategy(_strategy);
        }

        private ISettlementStrategy GetSettlementStrategy(StrategyType strategy)
        {
            ISettlementStrategy result = null;

            switch (strategy)
            {
                case StrategyType.MaxQuantity:
                    result = new StrategyMaxQuantity();
                    break;
                case StrategyType.MaxValue:
                    result = new StrategyMaxValue();
                    break;
                case StrategyType.MaxProfit:
                    result = new StrategyMaxProfit();
                    break;
            }
            return result;
        }

        internal void RunSettlementJob(IModel channel)
        {
            List<Settlement> settlements = new List<Settlement>();
            DateTime processingTime = DateTime.Now;
            IList<Offer> offers = _repository.GetOffersByDateTime(processingTime);
            if (offers.Count > 0)
            {
                List<string> offerIds = offers.Select(s => s.OfferId).ToList();
                UpdateOfferStatus(offerIds, channel, OfferStatus.Processing);

                IList<Bid> bids = _repository.GetBidsByOfferId(offerIds);

                settlements = _settlementStrategy.ProcessSettlement(offers, bids, _repository);

                PublishSettlements(settlements, channel);

                UpdateOfferStatus(offerIds, channel, OfferStatus.Settled);
            }
        }

        private void PublishSettlements(List<Settlement> settlements, IModel channel)
        {
            byte[] messageBody = Encoding.Default.GetBytes(JsonConvert.SerializeObject(settlements));
            channel.BasicPublish(Exchanges.UBID_Exchange, RoutingKeys.Settlement_Result, null, messageBody);
        }

        private void UpdateOfferStatus(List<string> offerIds, IModel channel, OfferStatus status)
        {
            _repository.SetOffersStatus(offerIds, status);
            PublishSettlementStatus(offerIds, channel, status);
        }

        private void PublishSettlementStatus(List<string> offerIds, IModel channel, OfferStatus status)
        {
            string ids = string.Join(",", offerIds);
            byte[] messageBody = Encoding.Default.GetBytes(JsonConvert.SerializeObject(new { OfferIds = ids, Status = status }));
            channel.BasicPublish(Exchanges.UBID_Exchange, RoutingKeys.Settlement_Status, null, messageBody);

        }
    }
}
