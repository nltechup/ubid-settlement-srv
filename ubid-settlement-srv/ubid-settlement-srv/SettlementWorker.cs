
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using ubid_settlement_common.Enums;
using ubid_settlement_common.Helpers;
using ubid_settlement_common.Interfaces;
using ubid_settlement_common.Models;
using ubid_settlement_common.RabbitMQ;
using ubid_settlement_repository;
using ubid_settlement_srv.Consumers;
using ubid_settlement_srv.Listeners;
using ubid_settlement_srv.Options;

namespace ubid_settlement_srv
{
    public class SettlementWorker : BackgroundService, ISettlementJob
    {
        private readonly ILogger<ISettlementJob> _logger;
        private readonly IServiceProvider _serviceProvider;
        private readonly ISettlementRepository _repository;
        private readonly IMapper _mapper;
        private int _minutesToRetry;
        private IModel _channel;
        private StrategyType _strategy;
        //private IRabbitMQService _rabbitMQService;

        public SettlementWorker(ILogger<ISettlementJob> logger, IServiceProvider serviceProvider, SettlementSettings settings,
            QueueSettings queueSettings, ISettlementRepository repository, IMapper mapper) //, IRabbitMQService rabbitMQService) //, IModel channel)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
            _repository = repository;
            _mapper = mapper;

            //Settlementsettings _settlementSettings = new Settlementsettings();
            //configuration.Bind("SettlementSettings", _settlementSettings);

            _minutesToRetry = settings.MinutesBeforeCheck * 60000;
            _strategy = StrategyHelper.GetStrategy(settings.StrategyType);

            var factory = new ConnectionFactory()
            {
                HostName = queueSettings.Rabbit_HostName,
                Port = queueSettings.Rabbit_Port,
                UserName = queueSettings.Rabbit_Username,
                Password = queueSettings.Rabbit_Password
            };
            RabbitMQHelper.WaitForRabbitMq(factory);

            IConnection _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //Validate RabbitMq infrastructure
            _serviceProvider.GetRequiredService<RabbitMQValidations>().ValidateQueues();
            // start listeners on rabbitmq queues
            _serviceProvider.GetRequiredService<OfferCreateListener>().Listen();
            _serviceProvider.GetRequiredService<OfferUpdateListener>().Listen();
            _serviceProvider.GetRequiredService<OfferDeleteListener>().Listen();

            _serviceProvider.GetRequiredService<GenericListener<BidCreateConsumer>>().Listen();
            _serviceProvider.GetRequiredService<GenericListener<BidUpdateConsumer>>().Listen();
            _serviceProvider.GetRequiredService<GenericListener<BidDeleteConsumer>>().Listen();

            //start worker process to execute a time base scheduler
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Start new Worker process at: {time}", DateTimeOffset.Now);
                SettlementJob job = new SettlementJob(_logger, _repository, _mapper, _strategy);
                job.RunSettlementJob(_channel);
                await Task.Delay(_minutesToRetry, stoppingToken);
            }
        }

    }
}
