﻿using System.Collections.Generic;
using System.Linq;
using ubid_settlement_common.Enums;
using ubid_settlement_data;
using ubid_settlement_repository;
using ubid_settlement_srv.Interfaces;

namespace ubid_settlement_srv.Strategies
{
    public class StrategyMaxQuantity : ISettlementStrategy
    {
        public List<Settlement> ProcessSettlement(IList<Offer> offers, IList<Bid> bids, ISettlementRepository _repository)
        {
            List<Settlement> result = new List<Settlement>();

            foreach (var offer in offers.OrderBy(o => o.EndAt))
            {
                Settlement newSettlement = new Settlement();
                newSettlement.OfferId = offer.OfferId;
                foreach (var bid in bids.Where(w => w.OfferId == offer.OfferId).OrderByDescending(o => o.Quantity))
                {
                    if (bid.Price >= offer.Price)
                    {
                        offer.Remaining = offer.Remaining - bid.Quantity;
                        newSettlement.OfferQuantity += bid.Quantity;

                        SettledBids newSettledBids = new SettledBids()
                        {
                            BidId = bid.BidId,
                            Quantity = bid.Quantity,
                            Price = bid.Price
                        };
                        bid.BidStatus = BidStatus.Settled;
                        newSettlement.Bids.Add(newSettledBids);
                        _repository.UpdateBid(bid);
                    }
                }

                result.Add(newSettlement);

                if (newSettlement.Bids.Count == 0)
                {
                    offer.OfferStatus = OfferStatus.Expired;
                }
                _repository.UpdateOffer(offer);
            }
            return result;
        }


    }
}
